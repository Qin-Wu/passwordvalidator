package password;
/*
 * @author Qin Wu, 991520749
 * This class validates password and it will be developed using TDD
 */
import static org.junit.Assert.*;
import org.junit.Test;

public class PasswordValidatorTest {
	
	@Test
	public void testHasValidCaseCharsRegular() {
		boolean result=PasswordValidator.hasValidCaseChars("aaAaaAaaa");
		assertTrue("Invalid case characters",result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionBlank() {
		boolean result=PasswordValidator.hasValidCaseChars("");
		assertFalse("Invalid case characters",result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull() {
		boolean result=PasswordValidator.hasValidCaseChars(null);
		assertFalse("Invalid case characters",result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNumber() {
		boolean result=PasswordValidator.hasValidCaseChars("75645475");
		assertFalse("Invalid case characters",result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		boolean result=PasswordValidator.hasValidCaseChars("EEEEEE");
		assertFalse("Invalid case characters",result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		boolean result=PasswordValidator.hasValidCaseChars("eeeeee");
		assertFalse("Invalid case characters",result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		boolean result=PasswordValidator.hasValidCaseChars("aA");
		assertTrue("Invalid case characters",result);
	}

	@Test
	public void testHasEnoughDigits() {
		assertTrue("Password does not have enough digits.",PasswordValidator.hasEnoughDigits("AAa8bcdefg1"));
	}
	
	@Test
	public void testHasEnoughDigitsException() {
		assertFalse("Password does not have enough digits.",PasswordValidator.hasEnoughDigits(null));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		assertTrue("Password does not have enough digits.",PasswordValidator.hasEnoughDigits("abcdef12"));
	}
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		assertFalse("Password does not have enough digits.",PasswordValidator.hasEnoughDigits("abcdefg2"));
	}
	@Test
	public void testIsValidLength() {
		assertTrue("Invalid password length",PasswordValidator.isValidLength("1234567890"));
	}
	
	@Test
	public void testIsValidLengthException() {
		assertFalse("Invalid password length",PasswordValidator.isValidLength(null));
	}
	
	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("Invalid password length",PasswordValidator.isValidLength("       "));
	}
	
	@Test
	public void testIsValidLengthBoundryIn() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("76543210"));
	}
	
	@Test
	public void testIsValidLengthBoundryOut() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("7654321"));		
	}
	

}
